/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

function getRandomColor() {
  let color = Math.floor(Math.random() * 0xffffff);
  return `#${(000000+color.toString(16)).substr(-6)}`;
}

const parentElement = document.getElementById("app");

const newBlock = document.createElement("div");

      newBlock.style = "width: 100%; height: 500px;  margin-bottom: 30px; display: flex; justify-content: center; align-items:center;";
      parentElement.appendChild(newBlock);

const buttonForColorChange = document.createElement("button");

      buttonForColorChange.innerText = "Change color";
      buttonForColorChange.style =
        "border: none; background: tomato; color: white; width: 200px; height: 60px";
      newBlock.appendChild(buttonForColorChange);

buttonForColorChange.addEventListener("click", function changeColorOnClick(e) {
  window.location.reload(true);
});

document.addEventListener("DOMContentLoaded", function() {
  newBlock.style.backgroundColor = getRandomColor();;
})
